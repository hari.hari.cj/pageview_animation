import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pageview_roll/resources/app_images.dart';
import 'package:pageview_roll/resources/app_strings.dart';
import 'package:pageview_roll/widgets/card_background_image_widget.dart';
import 'package:pageview_roll/widgets/card_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = PageController(viewportFraction: 0.7);

  late int currentPage = 0;
  double pagePosition = 0;


  @override
  void initState() {
    controller.addListener(listenPagePosition);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          BackgroundImage(),
          Positioned.fill(
            top: 150,
            left: 0,
            child: PageView.builder(
              itemBuilder: (BuildContext context, int index) {
                var pagePos = 1 - (pagePosition - index);

                double finalValue = pagePos.clamp(0.0, 1.0);
                double offset = 0;
                double value = 0;

                if (currentPage == index) {
                  offset = -50 * finalValue;
                  value = pagePos.clamp(0.0, 1.0);
                } else {
                  offset = 0.0;
                  value = pagePos.clamp(0.0, 1.0);
                }
                return CardWidget(offset: offset,value: value,);
              },
              reverse: false,
              controller: controller,
              itemCount: 30,
            ),
          ),
          userInfo(),
        ],
      ),
      extendBody: true,
      bottomNavigationBar: Container(
        height: 65,
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(.2),
                blurRadius: 10,
                offset: Offset(1, 5),
                spreadRadius: 4)
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
          image(AppImages.homepage_icon),
            image(AppImages.search_icon),
            image(AppImages.bill_icon),

          ],
        ),
      ),
    );
  }





  listenPagePosition() {

    setState(() {
      currentPage = controller.page!.round();
      pagePosition = controller.page!;
    },);
  }


  userInfo()
  {
    return Positioned.fill(
        top: 60,
        left: 0,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 70,
              width: 100,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: Image.network(
                    AppImages.user_profile,
                    scale: 1,
                  ).image,
                ),
                shape: BoxShape.circle,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
                AppStrings.user_name,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                  fontSize: 16),
            )
          ],
        ));
  }

  image(String imgUrl)
  {
    return Image(
      width: 30,
      height: 30,
      fit: BoxFit.cover,
      image: AssetImage(imgUrl),
    );
  }

}
