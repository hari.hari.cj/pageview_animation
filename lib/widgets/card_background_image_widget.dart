import 'package:flutter/material.dart';
import 'package:pageview_roll/resources/app_images.dart';

class BackgroundImage extends StatelessWidget {
  const BackgroundImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return   Positioned.fill(
      child: Image(
        fit: BoxFit.fitHeight,
        image: NetworkImage(
            AppImages.backgorund_image),
      ),
    );
  }
}
