import 'package:flutter/material.dart';
import 'package:pageview_roll/resources/app_images.dart';
import 'package:pageview_roll/resources/app_strings.dart';

class CardWidget extends StatefulWidget {
  double offset;
  double value;
  CardWidget({required this.offset,required this.value});

  @override
  _CardWidgetState createState() => _CardWidgetState();
}

class _CardWidgetState extends State<CardWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Transform.translate(
        offset: Offset(0, widget.offset),
        child: Container(
          height: 400,
          width: 250,

          // margin: EdgeInsets.symmetric(horizontal: 10),
          transformAlignment: Alignment.center,
          alignment: Alignment.center,

          child: Column(
            //   mainAxisAlignment: MainAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Transform.scale(
                  origin: Offset(0, 0),
                  scale: widget.value,
                  child: Transform.translate(
                    offset: Offset(0, widget.offset),
                    child: Opacity(
                      opacity: widget.value,
                      child: Image(
                        fit: BoxFit.fitWidth,
                        width: 200,
                        height: 130,
                        image: AssetImage(AppImages.car),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Transform.translate(
                  offset: Offset(0, widget.offset),
                  child: Opacity(
                    opacity: widget.value,
                    child: Text(
                        AppStrings.headline,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 25),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(0, widget.offset),
                  child: Opacity(
                    opacity: widget.value,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20),
                      child: Text(
                        AppStrings.body_content,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black.withOpacity(.5),
                            fontSize: 15,
                            letterSpacing: .12),
                      ),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(0, widget.offset),
                  child: Opacity(
                    opacity: widget.value,
                    child: Container(
                      width: 150,
                      height: 45,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Color(0xFF24203c),
                          borderRadius: BorderRadius.circular(25)),
                      child: Text(
                        AppStrings.button_text,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1.5,
                        ),
                      ),
                    ),
                  ),
                ),
              ]),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(.2),
                  blurRadius: 10,
                  offset: Offset(1, 5),
                  spreadRadius: 4),
            ],
            borderRadius: BorderRadius.circular(20),
          ),
        ),
      ),
    );
  }
}
